﻿using System;

namespace CTCLogging
{
    public interface IAppLogger
    {
        string LoggerPath { get; set; }
        string LogProfile { get; set; }
        string LogDetails { get; set; }
        string LogError { get; set; }
        string LogPath { get; set; }
        string LogProcess { get; set; }
        DateTime LogTime { get; set; }

        void AddLog();
        void Dispose();
    }
}