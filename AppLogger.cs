﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Xml.Serialization;

namespace CTCLogging
{
    public class AppLogger : IAppLogger, IDisposable
    {
        public string LoggerPath { get; set; }
        public string LogProfile { get; set; }
        public DateTime LogTime { get; set; }
        public string LogProcess { get; set; }
        public string LogDetails { get; set; }
        public string LogError { get; set; }
        public string LogPath { get; set; }


        private const int NumberOfRetries = 3;
        private const int DelayOnRetry = 1000;

        public AppLogger(string logPath)
        {
            LogPath = logPath;
        }
        public AppLogger(string logPath, string profile, string details, string process, DateTime time, string error, string path)
        {
            LoggerPath = logPath;
            LogProfile = profile;
            LogTime = time;
            LogProcess = process;
            LogDetails = details;
            LogPath = path;
            LogError = error;
        }

        public void AddLog()
        {
            Logging log = new Logging();
            if (!File.Exists(LoggerPath))
            {
                CreateLog(LoggerPath);
            }


            for (int i = 1; i <= NumberOfRetries; ++i)
            {
                try
                {
                    using (FileStream fStream = new FileStream(LoggerPath, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        XmlSerializer xLog = new XmlSerializer(typeof(Logging));
                        log = (Logging)xLog.Deserialize(fStream);
                        fStream.Close();
                    }
                    break; // When done we can break loop
                }
                catch (IOException) when (i <= NumberOfRetries)
                {

                    Thread.Sleep(DelayOnRetry);
                }
            }



            List<LoggingLog> logs = new List<LoggingLog>();
            logs = log.Items.ToList();
            LoggingLog logEntry = new LoggingLog
            {
                Time = LogTime.ToString("dd/MM/yyyy:hh:mm:ss"),
                ProfileDescription = LogProfile,
                Operation = LogProcess,
                Details = LogDetails,
                Error = LogError,
                Path = LogPath
            };
            logs.Add(logEntry);
            log.Items = logs.ToArray();
            try
            {
                using (Stream outLog = File.Open(LoggerPath, FileMode.OpenOrCreate, FileAccess.Write, FileShare.Read))
                {
                    StringWriter writer = new StringWriter();
                    XmlSerializer xSer = new XmlSerializer(typeof(Logging));
                    xSer.Serialize(outLog, log);
                    outLog.Flush();
                    outLog.Close();
                }
            }
            catch (Exception)
            {

            }



        }

        private void CreateLog(string logPath)
        {
            Logging log = new Logging();
            LoggingLog logEntry = new LoggingLog
            {
                ProfileDescription = "",
                Time = DateTime.Now.ToString("dd/MM/yyyy:hh:mm:ss"),
                Details = "Log File Created",
                Error = "",
                Operation = "Logging Started",
                Path = ""
            };
            List<LoggingLog> logs = new List<LoggingLog>
            {
                 logEntry
            };
            log.Items = logs.ToArray();

            for (int i = 1; i <= NumberOfRetries; ++i)
            {
                try
                {
                    using (Stream outLog = File.Open(logPath, FileMode.Create))
                    {
                        StringWriter writer = new StringWriter();
                        XmlSerializer xSer = new XmlSerializer(typeof(Logging));
                        xSer.Serialize(outLog, log);
                        outLog.Flush();
                        outLog.Close();
                    }
                    break; // When done we can break loop
                }
                catch (IOException) when (i <= NumberOfRetries)
                {

                    Thread.Sleep(DelayOnRetry);
                }
            }
        }

        bool disposed = false;
        // Instantiate a SafeHandle instance.
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        // Public implementation of Dispose pattern callable by consumers.
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
            {
                return;
            }

            if (disposing)
            {
                handle.Dispose();
                // Free any other managed objects here.
                //
            }

            disposed = true;
        }
    }
}
